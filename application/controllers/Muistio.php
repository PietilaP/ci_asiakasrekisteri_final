<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistio extends CI_Controller {
        public function __construct() {
                parent::__construct();
                $this->load->model('muistio_model');
                $this->load->library('util');
        }
        
        public function index($asiakas_id) {
            $this->session->set_userdata('asiakas_id',$asiakas_id);
            $data['muistiot'] = $this->muistio_model->hae_kaikki($asiakas_id);
            $data['paivays'] = date('d.m.Y H.i', time());
            $data['main_content'] = 'muistio_view';
            $this->load->view('template',$data);
        }
        
        public function tallenna() {
            $data = array(
                    'tallennettu' => $this->util->format_fin_to_sqldate($this->input->post('paivays')),
                    'teksti' => $this->input->post('teksti'),
                    'asiakas_id' => $this->session->userdata('asiakas_id')
                    
            );
                $this->muistio_model->lisaa($data);
                redirect('muistio/index/' . $this->session->userdata('asiakas_id'));
                        
            $data['main_content'] = 'asiakas_view';
        }
        
        public function poista($id) {
            $this->muistio_model->poista(intval($id));
            redirect('muistio/index/' . $this->session->userdata('asiakas_id'));
        }
}
