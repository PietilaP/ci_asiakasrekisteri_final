<h3>Muistio</h3>
<form action="<?php print site_url() . '/muistio/tallenna'; ?>" method="post">
    <div class="form-group">
        <label>Päiväys</label>
        <input name="paivays" class="form-control" value="<?php print($paivays);?>">
    </div>
    <div class="form-group">
        <label>Teksti</label>
        <textarea name="teksti" class="form-control" cols="40" rows="5"></textarea>
    </div>
    <div>
        <button>Tallenna</button>
    </div>
</form>

<table class="table">
    <tr>
        <th>ID</th>
        <th>Tallennettu</th>
        <th>Teksti</th>
        <th></th>
    </tr>
<?php
foreach ($muistiot as $muistio) {
    print "<tr>";
    print "<td>$muistio->id</td>";
    print "<td>";
    printf($this->util->format_sqldate_to_fin($muistio->tallennettu));
    print "</td>";
    print "<td>$muistio->teksti</td>";
    print "<td>" . anchor("muistio/poista/$muistio->id","Poista",array('onclick' => "return confirm('Do you want delete this record')")) . "</td>";
    print "</tr>";
    
}
?>