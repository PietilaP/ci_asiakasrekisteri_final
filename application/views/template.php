<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Asiakasrekisteri</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="<?php print base_url() . '/css/style.css';?>" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="#">Asiakasrekisteri</a>
              </div>
              <ul class="nav navbar-nav">
                <li class="active"><a href="<?php print site_url() . '/asiakas';; ?>">Home</a></li>
                <?php
                if ($this->session->has_userdata('kayttaja')) {
                ?>
                <li><a href="<?php print site_url() . 'kayttaja/kirjaudu_ulos'; ?>">Kirjaudu ulos</a></li>
                <?php
                }
                ?>
              </ul>
            </div>
        </nav>
        
        <div class="container">
        <h1>Asiakasrekisteri</h1>
        <?php
        $this->load->view($main_content);
        ?>
        </div>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?php print base_url() . 'js/asiakas.js';?>"></script>
    </body>
</html>
